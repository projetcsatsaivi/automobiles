package com.saivi.automobiles.domain;

import com.saivi.automobiles.model.Merchant;
import com.saivi.automobiles.repository.MerchantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MerchantDomain {

    @Autowired
    private MerchantRepository merchantRepo;

    public List<Merchant> fetchAllmerchant(){
       return merchantRepo.findAll();
    }

    public Merchant save(Merchant merchant){
        return merchantRepo.save(merchant);
    }

}
