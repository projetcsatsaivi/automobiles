package com.saivi.automobiles.domain;

import com.saivi.automobiles.model.Stock;
import com.saivi.automobiles.repository.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class StockDomain {

    @Autowired
    private StockRepository stockRepo;

    public List<Stock> getStock(){
        List<Stock> st = stockRepo.findAll();
        return st;
    }

    public Stock save(Stock stock){
        Stock st = stockRepo.save(stock);
        return st;
    }

}
