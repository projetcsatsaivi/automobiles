package com.saivi.automobiles;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.saivi")
public class AutomobilesApplication {

	public static void main(String[] args) {
		SpringApplication.run(AutomobilesApplication.class, args);
	}

}
