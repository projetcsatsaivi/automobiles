package com.saivi.automobiles.repository;

import com.saivi.automobiles.model.Stock;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface StockRepository extends MongoRepository<Stock, String> {
}
