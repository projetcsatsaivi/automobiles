package com.saivi.automobiles.repository;

import com.saivi.automobiles.model.Merchant;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MerchantRepository extends MongoRepository<Merchant,String> {
}
