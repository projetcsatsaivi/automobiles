package com.saivi.automobiles.controller;

import com.saivi.automobiles.domain.MerchantDomain;
import com.saivi.automobiles.model.Merchant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/merchant")
public class MerchantController {

    @Autowired
    private MerchantDomain merchantDomain;

    @RequestMapping(value = "/fetchAll",method = RequestMethod.GET)
    public List<Merchant> fetchAll (){
        return merchantDomain.fetchAllmerchant();
    }

    @RequestMapping(value = "/save",method = RequestMethod.POST)
    public Merchant save(@RequestBody Merchant merchant){
        return merchantDomain.save(merchant);
    }

}
