package com.saivi.automobiles.controller;

import com.saivi.automobiles.domain.StockDomain;
import com.saivi.automobiles.model.Stock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/stock")
public class StockController {

    @Autowired
    private StockDomain stDomain;

    @RequestMapping(value = "/getStock" ,method = RequestMethod.GET)
    public List<Stock> getAllStock(){
         return stDomain.getStock();
    }

    @RequestMapping(value = "/save" ,method = RequestMethod.POST)
    public Stock save(@RequestBody Stock stock){
        return stDomain.save(stock);
    }

}
